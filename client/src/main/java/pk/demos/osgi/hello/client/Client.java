package pk.demos.osgi.hello.client;


import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import pk.demos.osgi.hello.provider.Greeting;


public class Client implements BundleActivator{

    @Override
    public void start(BundleContext context) throws Exception {
        ServiceReference ref = context.getServiceReference(Greeting.class.getName());
        ((Greeting)context.getService(ref)).sayHello();
    }

    @Override
    public void stop(BundleContext context) throws Exception {
    }

}
