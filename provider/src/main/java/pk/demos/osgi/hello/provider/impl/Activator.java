package pk.demos.osgi.hello.provider.impl;


import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import pk.demos.osgi.hello.provider.Greeting;


public class Activator implements BundleActivator {

    @Override
    public void start(BundleContext ctx) throws Exception {
        System.out.println("Aktywuję powitanie");
        ctx.registerService(Greeting.class.getName(), new GreetingImpl("serwis"), null);
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        System.out.println("Dezaktywuję powitanie");
    }

}